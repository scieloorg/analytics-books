# coding: utf-8
import os
import unittest
from analytics_books.loadlogs import *


class LoadLogsTest(unittest.TestCase):

    def test_allowed_patterns_pdf_1(self):
        result = get_sbid_from_request('GET /id/wcgvd/pdf/moreira-9788575412374-16.pdf HTTP/1.1')

        self.assertEqual(result[0:2], ('wcgvd', 'pdf'))

    def test_allowed_patterns_pdf_2(self):

        result = get_sbid_from_request('GET /id/wc/pdf/moreira-9788575412374-16.pdf HTTP/1.1')

        self.assertEqual(result[0:2], ('wc', 'pdf'))

    def test_allowed_patterns_pdf_3(self):

        result = get_sbid_from_request('GET /id/wcgvdd/pdf/moreira-9788575412374-16.pdf HTTP/1.1')

        self.assertEqual(result[0:2], ('wcgvdd', 'pdf'))

    def test_allowed_patterns_pdf_4(self):
        result = get_sbid_from_request('GET /scielobooks/wcgvd/pdf/moreira-9788575412374-16.pdf HTTP/1.1')

        self.assertEqual(result[0:2], ('wcgvd', 'pdf'))

    def test_allowed_patterns_pdf_5(self):

        result = get_sbid_from_request('GET /id/wcgvdd/epub/moreira-9788575412374-16.epub HTTP/1.1')

        self.assertEqual(result[0:2], ('wcgvdd', 'epu'))

    def test_allowed_patterns_pdf_6(self):

        result = get_sbid_from_request('GET /id/wcgvd HTTP/1.1')

        self.assertEqual(result[0:2], ('wcgvd', 'htm'))

    def test_allowed_patterns_pdf_7(self):

        result = get_sbid_from_request('GET /id/wcgvdd HTTP/1.1')

        self.assertEqual(result[0:2], ('wcgvdd', 'htm'))

    def test_allowed_patterns_pdf_10(self):

        result = get_sbid_from_request('GET /id/w HTTP/1.1')

        self.assertEqual(result[0:2], ('w', 'htm'))

    def test_allowed_patterns_pdf_8(self):

        result = get_sbid_from_request('GET /id/wcgvdd/epub/moreira-9788575412374-16 HTTP/1.1')

        self.assertEqual(result[0:2], (None, None))

    def test_allowed_patterns_pdf_9(self):

        result = get_sbid_from_request('GET /id/wcgvdd/epub/moreira-9788575412374-16.epub HTTP/1.1')

        self.assertEqual(result[0:2], ('wcgvdd', 'epu'))

    def test_allowed_patterns_pdf_11(self):

        result = get_sbid_from_request('GET /id/w/epub/moreira-9788575412374-16.epub HTTP/1.1')

        self.assertEqual(result[0:2], ('w', 'epu'))

    def test_is_bot_1(self):

        result = is_bot('pingdom.com_bot_version_1.4_(http://www.pingdom.com/)')

        self.assertTrue(result)

    def test_is_bot_2(self):

        result = is_bot('Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)')

        self.assertFalse(result)
