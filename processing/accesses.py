# coding: utf-8

import os
import requests
import json
import argparse
import logging
import codecs
import datetime

import pymongo

import utils

MONGODB_DOMAIN = utils.settings['app:main'].get('mongodb_host', '127.0.0.1:27017').split(":")[0]
MONGODB_PORT = int(utils.settings['app:main'].get('mongodb_host', '127.0.0.1:27017').split(":")[1])
MONGODB_DBNAME = utils.settings['app:main'].get('mongodb_db', 'books')
BOOKS_API_HOST = utils.settings['app:main'].get('books_api_host', '127.0.0.1:5984').split(":")[0]
BOOKS_API_PORT = utils.settings['app:main'].get('books_api_host', '127.0.0.1:5984').split(":")[1]

logger = logging.getLogger(__name__)


def _config_logging(logging_level='INFO', logging_file=None):

    allowed_levels = {
        'DEBUG': logging.DEBUG,
        'INFO': logging.INFO,
        'WARNING': logging.WARNING,
        'ERROR': logging.ERROR,
        'CRITICAL': logging.CRITICAL
    }

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    logger.setLevel(allowed_levels.get(logging_level, 'INFO'))

    if logging_file:
        hl = logging.FileHandler(logging_file, mode='a')
    else:
        hl = logging.StreamHandler()

    hl.setFormatter(formatter)
    hl.setLevel(allowed_levels.get(logging_level, 'INFO'))

    logger.addHandler(hl)

    return logger


def get_books():
    url = 'http://{0}:{1}/scielobooks_1a/_design/scielobooks/_view/books'.format(BOOKS_API_HOST, BOOKS_API_PORT)
    try:
        jsondocs = requests.get(url).json()
    except:
        logger.error(u"Falha ao connectar na API de livros")
        exit()

    books = {}
    for reg in jsondocs['rows']:  # rows e uma array (lista) de registros no JSON
        book = books.setdefault(reg['id'], {})
        book['editor'] = reg['value']['publisher']
        book['title'] = reg['value']['title']
        book['eisbn'] = reg['value'].get('eisbn', '')

        creators = []
        if 'creators' in reg['value']:
            for creator in reg['value']['creators']:
                creators.append(creator[1][1])
        book['creators'] = '; '.join(creators)

        book['year'] = 'not defined'
        if 'year' in reg['value']:
            book['year'] = reg['value']['year'][0:10]

        book['creation_date'] = 'not defined'
        if 'creation_date' in reg['value']:
            book['creation_date'] = reg['value']['creation_date'][0:10]

    return books


def sumarize_by_month(stats):
    sumarized = {}
    for key, value in stats.items():

        month = key[4:10]
        if month:
            if key[:3] == 'htm':
                html_stats = sumarized.setdefault('htm', {})
                html_stats.setdefault(month, 0)
                html_stats[month] += value

            if key[:3] == 'pdf':
                pdf_stats = sumarized.setdefault('pdf', {})
                pdf_stats.setdefault(month, 0)
                pdf_stats[month] += value

            if key[:3] == 'epu':
                pdf_stats = sumarized.setdefault('epu', {})
                pdf_stats.setdefault(month, 0)
                pdf_stats[month] += value

    return sumarized


def get_collection():

    try:
        conn = pymongo.MongoClient(MONGODB_DOMAIN, MONGODB_PORT)
    except pymongo.errors.AutoReconnect:
        logger.error("MongoDB connection error")
        exit()

    db = conn[MONGODB_DBNAME + "_accesslog"]
    coll = db[MONGODB_DBNAME + "_analytics"]

    return coll


def fmt_json(data):
    output = {}

    output["sbid"] = data[0]
    output["eisbn"] = data[1]
    output["title"] = data[2]
    output["authors"] = data[3]
    output["published_at"] = data[4]
    output["created_at"] = data[5]
    output["editor"] = data[6]
    output["access_year"] = data[7]
    output["access_month"] = data[9]
    output["access_pdf"] = data[10]
    output["access_html"] = data[11]
    output["access_epub"] = data[12]
    output["access_total"] = data[13]

    return json.dumps(output)


def fmt_csv(data):
    return u','.join([u'"%s"' % unicode(i).replace('"', '""') for i in data])


def run(output_format, output_file):

    fmt = fmt_csv if output_format == 'csv' else fmt_json

    books = get_books()
    analytics = get_collection()

    report = codecs.open(output_file, 'w', encoding="utf-8")
    books_stats = analytics.find({'context': 'book'})

    stats_dict = {}
    for stat in books_stats:
        book_code = stat['code'].lower()
        stats_dict[book_code] = sumarize_by_month(stat)

    header = [
        u"sbid",
        u"eisbn",
        u"título",
        u"autores",
        u"publicado em",
        u"criado em",
        u"editor",
        u"ano de acesso",
        u"mês de acesso",
        u"data de acesso",
        u"acessos para pdf",
        u"acessos para html",
        u"acessos para epdf",
        u"total de acessos"
    ]

    if output_format == 'csv':
        report.write(u'%s\r\n' % u','.join([u'"%s"' % i for i in header]))

    for book_code in stats_dict:
        if not book_code in books:
            continue
        available_access_dates = set(stats_dict[book_code].get('pdf', {}).keys() + stats_dict[book_code].get('htm', {}).keys() + stats_dict[book_code].get('epu', {}).keys())
        for access_date in available_access_dates:
            accesses_pdf = stats_dict[book_code].get('pdf', {}).get(access_date, 0)
            accesses_html = stats_dict[book_code].get('htm', {}).get(access_date, 0)
            accesses_epdf = stats_dict[book_code].get('epu', {}).get(access_date, 0)
            accesses_total = accesses_pdf + accesses_html + accesses_epdf
            access_month = '-'.join([access_date[0:4],access_date[4:6],'01'])

            line = [
                book_code,
                books[book_code]['eisbn'],
                books[book_code]['title'],
                books[book_code]['creators'],
                books[book_code]['year'],
                books[book_code]['creation_date'],
                books[book_code]['editor'],
                access_date[:4],
                access_date[4:6],
                datetime.datetime.strptime(access_month, '%Y-%m-%d').isoformat(),
                accesses_pdf,
                accesses_html,
                accesses_epdf,
                accesses_total
            ]

            report.write(u'%s\r\n' % fmt(line))


def main():
    parser = argparse.ArgumentParser(description="Create an access report")

    parser.add_argument(
        '--format',
        '-f',
        choices=['json', 'csv'],
        default='json',
        help='Output format, should be json or csv'
    )

    parser.add_argument(
        '--output_file',
        '-r',
        default='report.txt',
        help='File name where the report will be stored'
    )

    parser.add_argument(
        '--logging_file',
        '-o',
        help='Full path to the log file'
    )

    parser.add_argument(
        '--logging_level',
        '-l',
        default='DEBUG',
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        help='Logggin level'
    )

    args = parser.parse_args()
    _config_logging(args.logging_level, args.logging_file)
    logger.info('Producing report')

    run(output_format=args.format, output_file=args.output_file)
